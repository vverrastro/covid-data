#!/usr/bin/python
from datetime import datetime, tzinfo, timedelta
import json, sys, getopt

def main(argv):

   inputfile = ''
   outputfile = ''
   type = ''

   try:
      opts, args = getopt.getopt(argv,"h:i:o:t:",["ifile=","ofile=","type="])
   except getopt.GetoptError:
      print('python update-data.py -i <inputfile> -o <[dpc-covid19-ita-province|dpc-covid19-ita-regioni|dpc-covid19-ita-andamento-nazionale.json]> -t <[denominazione_provincia|denominazione_regione|stato]>')
      sys.exit(2)

   for opt, arg in opts:
      if opt == '-h':
         print('python update-data.py -i <inputfile> -o <[dpc-covid19-ita-province|dpc-covid19-ita-regioni|dpc-covid19-ita-andamento-nazionale.json]> -t <[denominazione_provincia|denominazione_regione|stato]>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-t", "--type"):
         type = arg

   with open(inputfile, 'r', encoding='utf-8-sig') as infile:
       
      parsed_json = (json.load(infile))
      type_set = set()

      for element in parsed_json:
         type_set.add(element[type])

      for type_value in type_set:
        output_dict = convert_to_capsule_compliant_json(parsed_json, type, type_value)
        
        clean_type_value = clean_local_type(type_value)
        print_with_new_name(output_dict, outputfile, clean_type_value, type)

def clean_local_type(string):
    string = string.lower()
    string = string.replace("'", "")
    string = string.replace(" ", "")
    string = string.replace("/","")
    string = string.replace("-", "")
    string = string.replace("reggionell", "reggio")
    string = string.replace("reggiodi", "reggio")
    return string

def print_with_new_name(output_new, output_file,  new_name, type):
    if type == "stato":
        new_file_complete_path = output_file
    else:
        new_file_complete_path = output_file + '-' + new_name + '.json'
    with open(new_file_complete_path, 'w+', encoding='utf-8') as f_new:
        json.dump(output_new, f_new, ensure_ascii=False, indent=4)

def convert_to_capsule_compliant_json(json, type, type_value):
    
    converted_json = []

    for json_element in json:
        if json_element[type] == type_value:
            if 'T' in json_element['data']:
                json_element['data'] = from_utc_to_cet(json_element['data'])
            if type == 'stato':
                converted_item = {
                    "data": json_element["data"],
                    "stato": json_element["stato"],
                    "ricoveratiConSintomi": json_element["ricoverati_con_sintomi"],
                    "terapiaIntensiva": json_element["terapia_intensiva"],
                    "isolamentoDomiciliare": json_element["isolamento_domiciliare"],
                    "totaleOspedalizzati": json_element["totale_ospedalizzati"],
                    "totalePositivi": json_element["totale_positivi"],
                    "variazioneTotalePositivi": json_element["variazione_totale_positivi"],
                    "nuoviPositivi": json_element["nuovi_positivi"],
                    "dimessiGuariti": json_element["dimessi_guariti"],
                    "deceduti": json_element["deceduti"],
                    "totaleCasi": json_element["totale_casi"],
                    "tamponi": json_element["tamponi"],
                    "casiTestati": json_element["casi_testati"],
                    "note": json_element["note"],
                    "ingressiTerapiaIntensiva": json_element["ingressi_terapia_intensiva"],
                    "totalePositiviTestMolecolare": json_element["totale_positivi_test_molecolare"],
                    "totalePositiviTestMolecolareRapido": json_element["totale_positivi_test_antigenico_rapido"],
                    "tamponiTestMolecolare": json_element["tamponi_test_molecolare"],
                    "tamponiTestAntigenicoRapido": json_element["tamponi_test_antigenico_rapido"]
                }
            converted_json.append(converted_item)
    return converted_json

class CET(tzinfo):
    def utcoffset(self, dt):
        return timedelta(hours=1) + self.dst(dt)

    def dst(self, dt):
        dston = datetime(year=dt.year, month=3, day=29, hour=2, minute=0)
        dstoff = datetime(year=dt.year, month=10, day=25, hour=3, minute=0)
        if dston <= dt.replace(tzinfo=None) < dstoff:
            return timedelta(hours=1)
        else:
            return timedelta(0)

class UTC(tzinfo):
    def utcoffset(self, dt):
        return timedelta(0)

    def dst(self, dt):
        return timedelta(0)

def from_utc_to_cet(date):
    date_time_utc = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S").replace(tzinfo=UTC())
    date_time_cet = date_time_utc.astimezone(tz=CET())
    return date_time_cet.strftime('%Y-%m-%d %H:%M:%S')

if __name__ == "__main__":
   main(sys.argv[1:])
